/**************************
 Daniel Wilson
 CPSC 1021 / 003 / F20
 dpw2@g.clemson.edu
 Elliott McMillan / Victoria Xu
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>

using namespace std;


typedef struct Employee {

	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;

} employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}
void swap (int *x, int *y);



int main(int argc, char const *argv[]) {

  srand(unsigned (time(0)));


  int i;


    employee emp[10];

    cout << "Please enter employee information.\n\n";
    // For loop that initializes each employee.
    for (i = 0; i < 10; i++) {

        cout << "\t Enter employee's last name.\n";
        cin >> emp[i].lastName;


        cout << "\t Enter employee's first name.\n";
        cin >> emp[i].firstName;


        cout << "\t Enter employee's year of birth..\n";
        cin >> emp[i].birthYear;


        cout << "\t Enter employee's hourly wage.\n";
        cin >> emp[i].hourlyWage;


        if (i == 9) {

            break;

        }



    }



    // Implementation of random shuffle function.
    random_shuffle ( begin(emp), end(emp), myrandom);

    // Setting new array.
    employee empS[5] = {emp[0], emp[1], emp[2], emp[3], emp[4]};


    // sort(begin(emp), end(emp), name_order);



  return 0;


}


/*
 bool name_order(const employee& lhs, const employee& rhs) {

    if (lhs.lastName[0] > rhs.lastName[0]){

        return false;

    }

    else if ()
return true;

}

void swap (int *x, int *y) {

    int tempVal = *x;
    *x = *y;
    *y = tempVal;

}

*/
